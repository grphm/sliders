<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTables extends Migration {

    public function up() {

        Schema::create('solution_sliders', function(Blueprint $table) {

            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->string('title', 255)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_sliders');
    }
}

