<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersPhotosTables extends Migration {

    public function up() {

        Schema::create('solution_sliders_photos', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('slider_id', FALSE, TRUE)->index();
            $table->string('title', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->mediumText('content')->nullable();
            $table->string('link_src', 255)->nullable();
            $table->string('link_title', 255)->nullable();
            $table->string('image', 100)->nullable();
            $table->integer('order', FALSE, TRUE)->default(0)->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_sliders_photos');
    }
}

