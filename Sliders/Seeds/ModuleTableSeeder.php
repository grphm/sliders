<?php
namespace STALKER_CMS\Solutions\Sliders\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('solution_sliders_templates')->insert([
            'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Шаблон по умолчанию', 'en' => 'The default template', 'es' => 'La plantilla por defecto']),
            'path' => 'slider-single.blade.php', 'required' => TRUE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    private function translate(array $trans) {

        return array_first($trans, function($key, $value) {

            return $key == \App::getLocale();
        });
    }
}