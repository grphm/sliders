<?php
return [
    'list' => 'Lista de los deslizadores',
    'pictures' => 'Fotos',
    'edit' => 'Editar',
    'embed' => 'Código de inserción',
    'empty' => 'Lista está vacía',
    'missing' => 'Faltan imágenes',
    'pictures_count' => 'foto|fotos|fotos',
    'delete' => [
        'question' => 'Eliminar diapositiva',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Añadir una diapositiva',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres.',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar diapositiva',
        'form' => [
            'title' => 'Título',
            'document_help_description' => 'Es compatible con todos los formatos',
            'submit' => 'Guardar'
        ]
    ]
];