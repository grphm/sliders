<?php
return [
    'breadcrumb' => 'Fotos',
    'edit' => 'Editar',
    'blank' => 'Abrir en nueva ventana',
    'empty' => 'Lista está vacía',
    'delete' => [
        'question' => 'Eliminar diapositiva',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Añadir una diapositiva',
        'form' => [
            'title' => 'Título',
            'name' => 'Nombre',
            'content' => 'Contenido',
            'link_src' => 'Enlazar',
            'link_title' => 'Enlace de título',
            'document' => 'Documento',
            'image' => 'Imagen',
            'image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'document_select' => 'Selecto',
            'document_change' => 'Enmendar',
            'document_help_description' => 'Es compatible con todos los formatos',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: main',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar diapositiva',
        'form' => [
            'title' => 'Título',
            'name' => 'Nombre',
            'content' => 'Contenido',
            'link' => 'Enlazar',
            'document' => 'Documento',
            'image' => 'Imagen',
            'image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'document_select' => 'Selecto',
            'document_change' => 'Enmendar',
            'document_help_description' => 'Es compatible con todos los formatos',
            'submit' => 'Guardar'
        ]
    ]
];