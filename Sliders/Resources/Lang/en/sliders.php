<?php
return [
    'list' => 'List of sliders',
    'pictures' => 'Pictures',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'embed' => 'Embed code',
    'missing' => 'Images are missing',
    'pictures_count' => 'picture|pictures|pictures',
    'delete' => [
        'question' => 'Delete slider',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding slider',
        'form' => [
            'title' => 'Title',
            'content' => 'Content',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit news',
        'form' => [
            'title' => 'Title',
            'document_help_description' => 'Supported all formats',
            'submit' => 'Save'
        ]
    ]
];