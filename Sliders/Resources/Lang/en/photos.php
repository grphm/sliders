<?php
return [
    'breadcrumb' => 'Pictures',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'delete' => [
        'question' => 'Delete slide',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding slide',
        'form' => [
            'title' => 'Title',
            'name' => 'Name',
            'content' => 'Content',
            'link_src' => 'Link',
            'link_title' => 'Title link',
            'document' => 'Document',
            'image' => 'Picture',
            'image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit slide',
        'form' => [
            'title' => 'Title',
            'name' => 'Name',
            'content' => 'Content',
            'link' => 'Link',
            'document' => 'Document',
            'image' => 'Picture',
            'image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'submit' => 'Save'
        ]
    ]
];