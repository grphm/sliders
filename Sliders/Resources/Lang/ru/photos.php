<?php
return [
    'breadcrumb' => 'Изображения',
    'edit' => 'Редактировать',
    'empty' => 'Список пустой',
    'delete' => [
        'question' => 'Удалить слайд',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление слайда',
        'form' => [
            'title' => 'Название',
            'name' => 'Имя',
            'content' => 'Содержание',
            'link_src' => 'Ссылка',
            'link_title' => 'Название ссылки',
            'document' => 'Документ',
            'image' => 'Изображение',
            'image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'document_select' => 'Выбрать',
            'document_change' => 'Изменить',
            'document_help_description' => 'Поддерживаются все форматы',
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов. <br>Например: main',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование слайда',
        'form' => [
            'title' => 'Название',
            'name' => 'Имя',
            'content' => 'Содержание',
            'link' => 'Ссылка',
            'document' => 'Документ',
            'image' => 'Изображение',
            'image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'document_select' => 'Выбрать',
            'document_change' => 'Изменить',
            'document_help_description' => 'Поддерживаются все форматы',
            'submit' => 'Сохранить'
        ]
    ]
];