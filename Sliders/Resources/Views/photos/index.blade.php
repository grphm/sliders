@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.sliders.index') }}">
                <i class="{{ config('solutions_sliders::menu.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-wallpaper"></i> @lang('solutions_sliders_lang::photos.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-wallpaper"></i> @lang('solutions_sliders_lang::photos.breadcrumb')</h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_sliders', 'create', FALSE))
        @BtnAdd('solutions.sliders.photos_create', $slider->id)
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    {{ $slider->title }}
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @if($photos->count())
                    <div class="list-group js-nestable-list">
                        <ul class="p-l-0 menu-nestable-list">
                            @foreach($photos as $photo)
                                <li class="js-item-container list-group-item media" data-element="{!! $photo->id !!}">
                                    <div class="pull-left">
                                        <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
                                    </div>
                                    @if($photo->image && \Storage::exists($photo->image))
                                        <div class="pull-left">
                                            <img alt="{{ $photo->title }}" class="lv-img h-50 brd-0"
                                                 src="{{ asset('uploads/' . $photo->image) }}">
                                        </div>
                                    @endif
                                    <div class="pull-right">
                                        <div class="actions dropdown">
                                            <a aria-expanded="true" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('solutions.sliders.photos_edit', [$slider->id, $photo->id]) }}">@lang('solutions_sliders_lang::photos.edit')</a>
                                                </li>
                                                <li>
                                                    <a class="c-red js-item-remove" href="">
                                                        @lang('solutions_sliders_lang::photos.delete.submit')
                                                    </a>
                                                    {!! Form::open(['route' => ['solutions.sliders.photos_destroy', $slider->id, $photo->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                    <button type="submit"
                                                            data-question="@lang('solutions_sliders_lang::photos.delete.question') &laquo;{{ $photo->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('solutions_sliders_lang::photos.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('solutions_sliders_lang::photos.delete.cancelbuttontext')">
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">{{ $photo->title }}</div>
                                        <small class="lv-small">{{ $photo->content }}</small>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @else
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_sliders_lang::photos.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        var nestableList = [];
        var list = $('.menu-nestable-list').sortable({
            group: 'menu-nestable-list',
            delay: 500,
            handle: 'i.zmdi-swap-vertical',
            onDrop: function ($item, container, _super) {
                var serialize = list.sortable("serialize").get();
                $.ajax({
                    url: '{!! route('solutions.sliders.photos_sortable', $slider->id) !!}',
                    type: 'POST',
                    dataType: 'json',
                    data: {elements: JSON.stringify(serialize[0], null, '')},
                    beforeSend: function () {
                    },
                    success: function (response, textStatus, xhr) {
                        if (response.status == true) {
                            BASIC.notify(null, response.responseText, 'bottom', 'center', 'zmdi zmdi-notifications-none zmdi-hc-fw', 'success', 'animated flipInX', 'animated flipOutX');
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        BASIC.notify(null, xhr.responseText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                    }
                });
                _super($item, container);
            }
        });
    </script>
@stop