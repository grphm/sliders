@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.sliders.index') }}">
                <i class="{{ config('solutions_sliders::menu.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.title')) !!}
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.sliders.photos_index', $slider->id) }}">
                <i class="zmdi zmdi-wallpaper"></i> @lang('solutions_sliders_lang::photos.breadcrumb')
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('solutions_sliders_lang::photos.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('solutions_sliders_lang::photos.insert.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => ['solutions.sliders.photos_store', $slider->id], 'class' => 'form-validate', 'id' => 'add-solutions-slide-form', 'files' => TRUE]) !!}
                {!! Form::hidden('slider_id', $slider->id) !!}
                <div class="col-sm-8">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('name', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::photos.insert.form.name')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::textarea('content', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::photos.insert.form.content')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('link_src', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::photos.insert.form.link_src')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('link_title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::photos.insert.form.link_title')</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_sliders_lang::photos.insert.form.image')</p>
                            <small class="help-description">@lang('solutions_sliders_lang::photos.insert.form.image_help_description')</small>
                            <div class="fileinput fileinput-new m-t-5" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_sliders_lang::photos.insert.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_sliders_lang::photos.insert.form.image_change')</span>
                                        {!! Form::file('photo') !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_sliders_lang::photos.insert.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::photos.insert.form.title')</label>
                        </div>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_sliders_lang::photos.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        var form = $("#add-solutions-slide-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {title: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
    </script>
@stop