@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.sliders.index') }}">
                <i class="{{ config('solutions_sliders::menu.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_sliders_lang::sliders.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-edit"></i> @lang('solutions_sliders_lang::sliders.replace.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($slider, ['route' => ['solutions.sliders.update', $slider->id], 'class' => 'form-validate', 'id' => 'edit-solutions-sliders-form', 'method' => 'PUT']) !!}
                <div class="col-sm-6">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_sliders_lang::sliders.replace.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_articles_lang::rubrics.insert.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_sliders_lang::sliders.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        var form = $("#edit-solutions-sliders-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {
            title: {required: true},
            slug: {required: true, slug: true}
        };
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
    </script>
@stop