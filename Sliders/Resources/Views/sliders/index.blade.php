@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_sliders::menu.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_sliders::menu.icon') }}"></i>
            {!! array_translate(config('solutions_sliders::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_sliders', 'create', FALSE))
        @BtnAdd('solutions.sliders.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_sliders_lang::sliders.list')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($sliders as $slider)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{ route('solutions.sliders.photos_index', $slider->id) }}">
                                            @lang('solutions_sliders_lang::sliders.pictures')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('solutions_sliders', 'edit', FALSE))
                                        <li>
                                            <a href="{{ route('solutions.sliders.edit', $slider->id) }}">@lang('solutions_sliders_lang::sliders.edit')</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0);" class="js-copy-link"
                                           data-clipboard-text="{!! '@'.'Slider(\'' . $slider->slug . '\''.$slider->TemplateSlug.')' !!}">
                                            @lang('solutions_sliders_lang::sliders.embed')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('solutions_sliders', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_sliders_lang::sliders.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.sliders.destroy', $slider->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_sliders_lang::sliders.delete.question') &laquo;{{ $slider->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_sliders_lang::sliders.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_sliders_lang::sliders.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{{ $slider->title }}</div>
                            @if($slider->photos->count())
                                <small class="lgi-text">
                                    {!! $slider->photos->count() !!}
                                    {!! Lang::choice(Lang::get('solutions_sliders_lang::sliders.pictures_count'), $slider->photos->count()) !!}
                                </small>
                            @else
                                <small class="lgi-text">@lang('solutions_sliders_lang::sliders.missing')</small>
                            @endif
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_sliders_lang::sliders.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop