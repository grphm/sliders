@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_sliders::menu.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_sliders::menu.menu_child.templates.icon') }}"></i> {!! array_translate(config('solutions_sliders::menu.menu_child.templates.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_sliders::menu.menu_child.templates.icon') }}"></i>
            {!! array_translate(config('solutions_sliders::menu.menu_child.templates.title')) !!}
        </h2>
    </div>
    @BtnAdd('solutions.sliders.templates.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_sliders_lang::templates.templates')
                </div>
                <small class="lgi-text m-t-5">@lang('solutions_sliders_lang::templates.root_directory')/{!! $locale_prefix !!}</small>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($templates as $template)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('solutions.sliders.templates.edit', $template->id) !!}">
                                            @lang('solutions_sliders_lang::templates.edit')
                                        </a>
                                    </li>
                                    @if($template->required)
                                    @else
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_sliders_lang::templates.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.sliders.templates.destroy', $template->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_sliders_lang::templates.delete.question') &laquo;{{ $template->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_sliders_lang::templates.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_sliders_lang::templates.delete.cancelbuttontext')">
                                            </button>
                                        {!! Form::close() !!}
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-title">{!! $template->title !!}</div>
                            <small class="lgi-small"><strong>{!! $template->path !!}</strong></small>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_sliders_lang::templates.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop