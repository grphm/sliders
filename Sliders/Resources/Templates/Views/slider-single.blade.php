@set($slider, \PublicSliders::slider($slider_slug))
@if($slider->count())
    <ul>
        @foreach($slider as $slide)
            <li>
                <img src="{!! $slide->AssetImage !!}" alt="{{ $slide->name }}">
                <a title="{{ $slide->name }}" href="{!! $slide->LinkSource !!}">
                    {!! $slide->link_title !!}
                </a>
            </li>
        @endforeach
    </ul>
@endif