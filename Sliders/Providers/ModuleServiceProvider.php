<?php
namespace STALKER_CMS\Solutions\Sliders\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Solutions\Sliders\Http\Controllers\PublicSlidersController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_sliders_views');
        $this->registerLocalization('solutions_sliders_lang');
        $this->registerConfig('solutions_sliders::config', 'Config/sliders.php');
        $this->registerSettings('solutions_sliders::settings', 'Config/settings.php');
        $this->registerActions('solutions_sliders::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_sliders::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
    }

    public function register() {

        \App::bind('PublicSlidersController', function() {

            return new PublicSlidersController();
        });
    }

    /********************************************************************************************************************/
    protected function registerBladeDirectives() {

        \Blade::directive('Slider', function($expression) {

            if(Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if(!empty($expression)):
                $expressions = [];
                foreach(explode(',', $expression, 2) as $parameter):
                    $expressions[] = preg_replace("/[\']/i", '', trim($parameter));
                endforeach;
                switch(count($expressions)):
                    case 1:
                        if(view()->exists("home_views::slider-single")):
                            return "<?php echo \$__env->make('home_views::slider-single', ['slider_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                    case 2:
                        if(view()->exists("home_views::$expressions[1]")):
                            return "<?php echo \$__env->make('home_views::$expressions[1]', ['slider_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                endswitch;
            endif;
            return NULL;
        });
    }

    public function publishesTemplates() {

        $this->publishes([
            __DIR__.'/../Resources/Templates' => base_path('home/Resources')
        ]);
    }
}
