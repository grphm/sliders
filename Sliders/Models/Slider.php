<?php
namespace STALKER_CMS\Solutions\Sliders\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Slider extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_sliders';
    protected $fillable = ['slug', 'locale', 'template_id', 'title', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->locale = \App::getLocale();
        $this->template_id = $request::input('template_id');
        $this->title = $request::input('title');
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->template_id = $request::input('template_id');
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getTemplateSlugAttribute() {

        if(!empty($this->template)):
            if($this->template->required):
                return NULL;
            else:
                $locale_prefix = ($this->template->locale == settings(['core_system', 'settings', 'base_locale'])) ? '' : $this->template->locale.'.';
                return ', \''.$locale_prefix.substr($this->template->path, 0, -10).'\'';
            endif;
        endif;
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function photos() {

        return $this->hasMany('\STALKER_CMS\Solutions\Sliders\Models\SliderPhotos', 'slider_id', 'id');
    }

    /**
     * Шаблон слайдера
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Solutions\Sliders\Models\Template', 'id', 'template_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}