<?php
namespace STALKER_CMS\Solutions\Sliders\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Шаблон слайдера
 * Class Template
 * @package STALKER_CMS\Solutions\Sliders\Models
 */
class Template extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_sliders_templates';
    protected $fillable = ['locale', 'title', 'path', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->path = $request::input('path');
        $this->required = $request::input('required');
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->user_id = \Auth::user()->id;
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['title' => 'required', 'path' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}