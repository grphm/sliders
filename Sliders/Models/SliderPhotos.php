<?php
namespace STALKER_CMS\Solutions\Sliders\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class SliderPhotos extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_sliders_photos';
    protected $fillable = ['slider_id', 'title', 'content', 'link_src', 'link_title'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $this->slider_id = $request::input('slider_id');
        $this->title = $request::input('title');
        $this->name = $request::input('name');
        $this->content = $request::input('content');
        $this->link_src = $request::input('link_src');
        $this->link_title = $request::input('link_title');
        $this->image = $request::has('image') ? $request::input('image') : NULL;
        $this->order = $this::max('order') + 1;
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->name = $request::input('name');
        $model->content = $request::input('content');
        $model->link_src = $request::input('link_src');
        $model->link_title = $request::input('link_title');
        $model->image = $request::has('image') ? $request::input('image') : NULL;
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getAssetImageAttribute() {

        if($this->attributes['image']):
            return asset('uploads'.$this->attributes['image']);
        else:
            return NULL;
        endif;
    }

    public function getLinkSourceAttribute() {

        return !empty($this->attributes['link_src']) ? $this->attributes['link_src'] : 'javascript:void(0)';
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slider_id' => 'required', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}