<?php

\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {

    \Route::resource('sliders', 'SlidersController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.sliders.index',
                'create' => 'solutions.sliders.create',
                'store' => 'solutions.sliders.store',
                'edit' => 'solutions.sliders.edit',
                'update' => 'solutions.sliders.update',
                'destroy' => 'solutions.sliders.destroy'
            ]
        ]
    );
    \Route::resource('sliders.photos', 'SlidersPhotosController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.sliders.photos_index',
                'create' => 'solutions.sliders.photos_create',
                'store' => 'solutions.sliders.photos_store',
                'edit' => 'solutions.sliders.photos_edit',
                'update' => 'solutions.sliders.photos_update',
                'destroy' => 'solutions.sliders.photos_destroy'
            ]
        ]
    );
    \Route::resource('sliders/templates', 'TemplatesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.sliders.templates.index',
                'create' => 'solutions.sliders.templates.create',
                'store' => 'solutions.sliders.templates.store',
                'edit' => 'solutions.sliders.templates.edit',
                'update' => 'solutions.sliders.templates.update',
                'destroy' => 'solutions.sliders.templates.destroy'
            ]
        ]
    );
    \Route::post('sliders/{slider_id}/photos/sortable', ['as' => 'solutions.sliders.photos_sortable', 'uses' => 'SlidersPhotosController@sortable']);
});