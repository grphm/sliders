<?php
namespace STALKER_CMS\Solutions\Sliders\Http\Controllers;

use Illuminate\Support\Facades\View;
use STALKER_CMS\Solutions\Sliders\Models\Template;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Шаблон галерей
 * Class TemplatesController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class TemplatesController extends ModuleController implements CrudInterface {

    protected $model;
    protected $locale_prefix;

    /**
     * TemplatesController constructor.
     * @param Template $template
     */
    public function __construct(Template $template) {

        parent::__construct();
        $this->model = $template;
        \PermissionsController::allowPermission('solutions_sliders', 'templates');
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index() {

        $templates = $this->model->whereLocale(\App::getLocale())->orderBy('updated_at', 'DESC')->get();
        return view('solutions_sliders_views::templates.index', compact('templates'));
    }

    /**
     * @return View
     */
    public function create() {

        $template_content = '';
        if(view()->exists('solutions_sliders_views::templates.sketch')):
            $template_content = \File::get(view('solutions_sliders_views::templates.sketch')->getPath());
        endif;
        return view('solutions_sliders_views::templates.create', compact('template_content'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $template_directory = base_path('home/Resources/Views/'.$this->locale_prefix);
            if(\File::exists($template_directory) === FALSE):
                \File::makeDirectory($template_directory, 0754, TRUE);
            endif;
            $view_path = double_slash($template_directory.'/'.$request::input('path').'.blade.php');
            if(\File::exists($view_path) === FALSE):
                $request::merge(['path' => $request::input('path').'.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                return \ResponseController::success(1600)->redirect(route('solutions.sliders.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id) {

        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = realpath(base_path('/home/Resources/Views/'.$this->locale_prefix.$template->path));
        if(\File::exists($view_path)):
            $template_content = \File::get($view_path);
        endif;
        return view('solutions_sliders_views::templates.edit', compact('template_content', 'template'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = double_slash(base_path('home/Resources/Views/'.$this->locale_prefix.'/').$template->path);
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.sliders.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = realpath(base_path('/home/Resources/Views/'.$this->locale_prefix.'/'.$template->path));
            if(\File::exists($view_path)):
                \File::delete($view_path);
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('solutions.sliders.templates.index'))->json();
        } catch(\Exception $exception) {
            return \ResponseController::success(2503)->json();
        }
    }
}