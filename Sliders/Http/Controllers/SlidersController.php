<?php
namespace STALKER_CMS\Solutions\Sliders\Http\Controllers;

use STALKER_CMS\Solutions\Sliders\Models\Slider;
use STALKER_CMS\Solutions\Sliders\Models\Template;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class SlidersController extends ModuleController implements CrudInterface {

    protected $model;
    protected $template;

    public function __construct(Slider $slider, Template $template) {

        $this->model = $slider;
        $this->template = $template;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('solutions_sliders', 'sliders');
        $sliders = $this->model->whereLocale(\App::getLocale())->orderBy('created_at', 'DESC')->with(['photos' => function($query) {

            $query->orderBy('order');
        }])->get();
        return view('solutions_sliders_views::sliders.index', compact('sliders'));
    }

    public function create() {

        \PermissionsController::allowPermission('solutions_sliders', 'create');
        $templates = $this->template->whereLocale(\App::getLocale())->lists('title', 'id');
        if($templates->count()):
            return view('solutions_sliders_views::sliders.create', compact('templates'));
        else:
            return redirect()->route('solutions.sliders.templates.create')->with('status', 2616);
        endif;
    }

    public function store() {

        \PermissionsController::allowPermission('solutions_sliders', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $slider = $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.sliders.photos_index', $slider->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('solutions_sliders', 'edit');
        $slider = $this->model->findOrFail($id);
        $templates = $this->template->whereLocale(\App::getLocale())->lists('title', 'id');
        return view('solutions_sliders_views::sliders.edit', compact('slider', 'templates'));
    }

    public function update($id) {

        \PermissionsController::allowPermission('solutions_sliders', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.sliders.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('solutions_sliders', 'delete');
        \RequestController::isAJAX()->init();
        if($model = $this->model->whereFind(['id' => $id])):
            $this->deletePhotos($model);
            $model->delete();
        endif;
        return \ResponseController::success(1203)->redirect(route('solutions.sliders.index'))->json();
    }

    private function deletePhotos(Slider $slider) {

        foreach($slider->photos as $photo):
            if(!empty($photo->image) && \Storage::exists($photo->image)):
                \Storage::delete($photo->image);
            endif;
        endforeach;
    }
}