<?php
namespace STALKER_CMS\Solutions\Sliders\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

abstract class ModuleController extends Controller {

    function __construct() {

        $this->locale_prefix = (\App::getLocale() == settings(['core_system', 'settings', 'base_locale'])) ? '' : \App::getLocale().'/';
        view()->share('locale_prefix', $this->locale_prefix);
    }
}