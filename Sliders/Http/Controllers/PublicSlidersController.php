<?php
namespace STALKER_CMS\Solutions\Sliders\Http\Controllers;

use STALKER_CMS\Solutions\Sliders\Models\Slider;
use Illuminate\Database\Eloquent\Collection;

class PublicSlidersController extends ModuleController {

    protected $model;

    public function __construct() {

        $this->model = new Slider();
    }

    public function slider($slug) {

        $slider = $this->model->whereLocale(\App::getLocale())->whereSlug($slug)->with(['photos' => function($query) {

            $query->orderBy('order');
        }])->first();
        if($slider):
            if(count($slider->photos)):
                return $slider->photos;
            endif;
        endif;
        return new Collection();
    }
}