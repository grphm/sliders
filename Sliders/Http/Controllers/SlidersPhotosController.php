<?php
namespace STALKER_CMS\Solutions\Sliders\Http\Controllers;

use STALKER_CMS\Solutions\Sliders\Models\Slider;
use STALKER_CMS\Solutions\Sliders\Models\SliderPhotos;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class SlidersPhotosController extends ModuleController implements CrudInterface {

    protected $model;
    protected $slider;

    public function __construct(Slider $slider, SliderPhotos $photo) {

        $this->slider = $slider->whereLocale(\App::getLocale())->findOrFail(\Request::segment(3));
        $this->model = $photo;
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_sliders', 'sliders');
    }

    public function index() {

        return view('solutions_sliders_views::photos.index', [
            'photos' => $this->model->whereSliderId($this->slider->id)->orderBy('order')->get(),
            'slider' => $this->slider
        ]);
    }

    public function create() {

        return view('solutions_sliders_views::photos.create', [
            'slider' => $this->slider
        ]);
    }

    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if($image = $this->uploadImages($request)):
                $request::merge($image);
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.sliders.photos_index', $this->slider->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($gallery_id = NULL, $id = NULL) {

        return view('solutions_sliders_views::photos.edit', [
            'photo' => $this->model->findOrFail($id),
            'slider' => $this->slider
        ]);
    }

    public function update($gallery_id = NULL, $id = NULL) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $slide = $this->model->findOrFail($id);
            if($images = $this->uploadImages($request, $slide)):
                $request::merge($images);
            else:
                $request::merge(['image' => $slide->image]);
            endif;
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.sliders.photos_index', $this->slider->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($gallery_id = NULL, $id = NULL) {

        \RequestController::isAJAX()->init();
        $slide = $this->model->findOrFail($id);
        $this->deleteImage($slide);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.sliders.photos_index', $this->slider->id))->json();
    }

    public function sortable() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['elements' => 'required'])):
            $elements = json_decode($request::input('elements'), TRUE);
            foreach($elements as $index => $photo):
                $this->model->where('id', $photo['element'])->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**************************************************************************************************************/
    private function uploadImages(\Request $request, $photo = NULL) {

        if(!is_null($photo)):
            $images = ['image' => $photo->image];
        else:
            $images = ['image' => NULL];
        endif;
        if($request::hasFile('photo')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('photo')->getClientOriginalExtension();
            $photoPath = 'content/sliders';
            $request::file('photo')->move('uploads/'.$photoPath, $fileName);
            $images['image'] = add_first_slash($photoPath.'/'.$fileName);
            if(!empty($photo->image) && \Storage::exists($photo->image)):
                \Storage::delete($photo->image);
                $photo->image = NULL;
                $photo->save();
            endif;
        endif;
        return $images;
    }

    private function deleteImage(SliderPhotos $slide) {

        if(!empty($slide->image) && \Storage::exists($slide->image)):
            \Storage::delete($slide->image);
            $slide->image = NULL;
        endif;
        $slide->save();
    }
}