<?php
namespace STALKER_CMS\Solutions\Sliders\Facades;

use Illuminate\Support\Facades\Facade;

class PublicSliders extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicSlidersController';
    }
}