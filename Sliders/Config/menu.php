<?php
return [
    'package' => 'solutions_sliders',
    'title' => ['ru' => 'Слайды', 'en' => 'Sliders', 'es' => 'Diapositivas'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-collection-image',
    'menu_child' => [
        'sliders' => [
            'title' => ['ru' => 'Список', 'en' => 'List', 'es' => 'Lista'],
            'route' => 'solutions.sliders.index',
            'icon' => 'zmdi zmdi-view-list'
        ],
        'templates' => [
            'title' => ['ru' => 'Шаблоны', 'en' => 'Templates', 'es' => 'Plantillas'],
            'route' => 'solutions.sliders.templates.index',
            'icon' => 'zmdi zmdi-layers'
        ]
    ]
];