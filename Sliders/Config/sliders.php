<?php
return [
    'package_name' => 'solutions_sliders',
    'package_title' => ['ru' => 'Слайды', 'en' => 'Sliders', 'es' => 'Diapositivas'],
    'package_icon' => 'zmdi zmdi-collection-case-play',
    'relations' => ['core_content'],
    'package_description' => [
        'ru' => 'Позволяет управлять слайдами. Используется модуль "Контент"',
        'en' => 'It allows you to control sliders. Using the module "Content"',
        'es' => 'Se le permite controlar de diapositivas. Utilice el módulo de "Contenido"'
    ],
    'version' => [
        'ver' => 1.1,
        'date' => '23.01.2017'
    ]
];